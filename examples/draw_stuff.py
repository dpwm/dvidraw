# This is the first example

# Let's import the dvidraw module. This is a little slow, because it's creating
# a default texrunner instance, and this involves spawning latex
import dvidraw

# The cursor is where all the magic happens
c = dvidraw.Cursor()

# Let's draw a box now, 10cm * 10cm

c(-20, -20).to.mm(100, 100).rect()
c.text(r'Test \TeX', anchor=(0.5, 0.5))


# And let's show it in a GUI window
c.save.screen()

c.save('test.pdf')

# We might also be interested in saving it to a pdf or a png
#c.save('test.pdf')
#c.save('test.png', dpi=150)

# What you see in the png and what you see in the PDF should be the same as what
# you see on the screen.

# In fact, everything you draw is saved into a picture, which is stored on the
# cursor. A picture is an abstract representation of all the components on an
# image. It is only when you call save that these are drawn.

#c.clear()

# This should be an extension to make the drawing more useful

# Let's suppose you wish to draw some kind of plot

# So far we've only drawn something that could have been done easier in
# a general purpose package program like inkscape.

# Let's focus on something a little more useful, such as an (accurate!)
# refraction diagram!

# The only piece of physics required here is Snell's law. That is, that
# n1*sin(theta2) = n2*sin(theta2)

# Hmm, tex arrows are somewhat nasty for diagrams! I guess we'll roll our own
# Let's run to some kind of boundary.
#c.cm(0, 0).bearing(-30).forward(x=-5).cw(180).to.forward(x=0).arrow('->')

#c.cw(30)
# Okay, now that we're there, let's come back again

# Now, what should be the required bearing?
# This seems a little futile!

